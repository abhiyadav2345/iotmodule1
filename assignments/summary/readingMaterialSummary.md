# **IIoT**
![](extras/iiot.png)<br>
The Industrial Internet of Things (IIoT) refers to interconnected sensors, instruments, and other devices networked together with computers' industrial applications, including manufacturing and energy management. This connectivity allows for data collection, exchange, and analysis, potentially facilitating improvements in productivity and efficiency as well as other economic benefits.<br>

## **Industrial Revolution**
![](extras/indrev.PNG)<br>
The industrial internet of things (IIoT) is the application of IoT technologies in manufacturing. Like the first industrial revolution in the 18th century, IIoT is transforming today’s manufacturing industry. This fourth industrial revolution is built on advancements in artificial intelligence (AI), IoT, 3D printing and robotics, and they’re the foundation for the factories of the future.<br>

- 1st industrial revolution – Steam and water power are used to mechanize production
- 2nd industrial revolution – Electricity allows for mass production with assembly lines
- 3rd industrial revolution – IT and computer technology are used to automate processes
- 4th industrial revolution (Industry 4.0) – Enhancing automation and connectivity with CPS<br>

## **Industry 3.0: Automated Production Using IT**
![](extras/ind3.jpg)<br>
It began with the first computer era. These early computers were often very simple, unwieldy and incredibly large relative to the computing power they were able to provide, but they laid the groundwork for a world today that one is hard-pressed to imagine without computer technology.<br>
Industry 3.0 introduced more automated systems onto the assembly line to perform human tasks, i.e. using Programmable Logic Controllers (PLC). Although automated systems were in place, they still relied on human input and intervention.<br>

### **What is the difference between Industry 3.0 and Industry 4.0?**

In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor.<br>

## **Typical Industry 3.0 Architecture**
![](extras/art3.PNG)<br>
Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts. <br>

## **Industry 4.0: Cyber-Physical Systems**
![](extras/ind4.PNG)<br>
The Fourth industrial Revolution is the era of smart machines, storage systems and production facilities that can autonomously exchange information, trigger actions and control each other without human intervention.<br>
This exchange of information is made possible with the Industrial Internet of things (IIoT) as we know it today. Key elements of Industry 4.0 include:<br>

- Cyber-physical system — a mechanical device that is run by computer-based algorithms.
- The Internet of things (IoT) — interconnected networks of machine devices and vehicles embedded with computerized sensing, scanning and monitoring capabilities.
- Cloud computing — offsite network hosting and data backup.
- Cognitive computing — technological platforms that employ artificial intelligence.

## **Typical Industry 4.0 Architecture**
![](extras/ind4art.PNG)<br>
## **Iot Tools - Analyse your data using various Tools available online**
#### **IoT TSDB tools - Store your data in Time series databases**
- Prometheus
- InfluxDB
#### **IoT Dashboards - View all your data into beautiful dashboards**
- Grafana
- Thingsboard
#### **IOT Platforms - Analyse your data on these platforms**
- AWS IOT
- Google IoT
- Azure IoT
- Thingsboard
#### **Get Alerts - Get Alerts based on your data using these platforms**
- Zaiper
- Twilio
